# Stuffo Inventory

Fork this and start building your inventory.
It's simple.
If you don't want your inventory to be public, keep your fork private.

You can look in the `test` branch for an example.
